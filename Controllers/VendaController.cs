using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;

namespace tech_test_payment_api.Controllers
{
    public class VendaController : Controller
    {
        private readonly PaymentContext _context;

        public VendaController(PaymentContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var vendas = _context.Vendas.ToList();
            return View(vendas);
        }

        public IActionResult ViewVendedor(int id)
        {
            var vendedores = _context.Vendedores.ToList();
            return View(vendedores);
        }

        public IActionResult ViewItem(int id)
        {
            var itens = _context.Itens.ToList();
            return View(itens);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Venda venda)
        {
            venda.Status = EnumStatusVenda.Aguardando;
            venda.Data = DateTime.Now;

            var vendedor = _context.Vendedores.Find(venda.VendedorId);
            if (vendedor == null)
                return RedirectToAction(nameof(Index));

            var item = _context.Itens.Find(venda.ItemId);
            if (item == null)
                return RedirectToAction(nameof(Index));

            _context.Vendas.Add(venda);
            _context.SaveChanges();

            return RedirectToAction(nameof(Index));
        }

        public IActionResult CreateVendedor()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateVendedor(Vendedor vendedor)
        {
            _context.Vendedores.Add(vendedor);
            _context.SaveChanges();

            return RedirectToAction(nameof(ViewVendedor));
        }

        public IActionResult CreateItem()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateItem(Item item)
        {
            _context.Itens.Add(item);
            _context.SaveChanges();

            return RedirectToAction(nameof(ViewItem));
        }

        public IActionResult Read(int id)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null)
                return RedirectToAction(nameof(Index));
            return View(venda);
        }

        public IActionResult ReadVendedor(int id)
        {
            var vendedor = _context.Vendedores.Find(id);
            if (vendedor == null)
                return RedirectToAction(nameof(ViewVendedor));
            return View(vendedor);
        }

        public IActionResult ReadItem(int id)
        {
            var item = _context.Itens.Find(id);
            if (item == null)
                return RedirectToAction(nameof(ViewItem));
            return View(item);
        }

        public IActionResult Update(int id)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null)
                return RedirectToAction(nameof(Index));
            return View(venda);
        }

        [HttpPost]
        public IActionResult Update(Venda venda)
        {
            var vendaDb = _context.Vendas.Find(venda.Id);

            if(vendaDb.Status == EnumStatusVenda.Aguardando){
                if(venda.Status == EnumStatusVenda.Aprovado){
                    vendaDb.Status = EnumStatusVenda.Aprovado;
                }else if(venda.Status == EnumStatusVenda.Cancelado){
                    vendaDb.Status = EnumStatusVenda.Cancelado;
                }else{
                    return RedirectToAction(nameof(Index));
                }
            } else if (vendaDb.Status == EnumStatusVenda.Aprovado){
                if(venda.Status == EnumStatusVenda.Cancelado){
                    vendaDb.Status = EnumStatusVenda.Cancelado;
                }else if(venda.Status == EnumStatusVenda.Enviado){
                    vendaDb.Status = EnumStatusVenda.Enviado;
                }else{
                    return RedirectToAction(nameof(Index));
                }
            } else if (vendaDb.Status == EnumStatusVenda.Enviado){
                if(venda.Status == EnumStatusVenda.Entregue){
                    vendaDb.Status = EnumStatusVenda.Entregue;
                }else{
                    return RedirectToAction(nameof(Index));
                }
            } else {
                return RedirectToAction(nameof(Index));
            }

            _context.Vendas.Update(vendaDb);
            _context.SaveChanges();
            return RedirectToAction(nameof(Index));
        }

        public IActionResult UpdateVendedor(int id)
        {
            var vendedor = _context.Vendedores.Find(id);
            if (vendedor == null)
                return RedirectToAction(nameof(ViewVendedor));
            return View(vendedor);
        }

        [HttpPost]
        public IActionResult UpdateVendedor(Vendedor vendedor)
        {
            var vendedorDb = _context.Vendedores.Find(vendedor.Id);
            
            vendedorDb.Cpf = vendedor.Cpf;
            vendedorDb.Nome = vendedor.Nome;
            vendedorDb.Email = vendedor.Email;
            vendedorDb.Telefone = vendedor.Telefone;

            _context.Vendedores.Update(vendedorDb);
            _context.SaveChanges();
            return RedirectToAction(nameof(ViewVendedor));
        }

        public IActionResult Delete(int id)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null)
                return RedirectToAction(nameof(Index));
            return View(venda);
        }

        [HttpPost]
        public IActionResult Delete(Venda venda)
        {
            var vendaDb = _context.Vendas.Find(venda.Id);
            _context.Vendas.Remove(vendaDb);
            _context.SaveChanges();
            return RedirectToAction(nameof(Index));
        }

        public IActionResult DeleteVendedor(int id)
        {
            var vendedor = _context.Vendedores.Find(id);
            if (vendedor == null)
                return RedirectToAction(nameof(ViewVendedor));
            return View(vendedor);
        }

        [HttpPost]
        public IActionResult DeleteVendedor(Vendedor vendedor)
        {
            var vendedorDb = _context.Vendedores.Find(vendedor.Id);
            _context.Vendedores.Remove(vendedorDb);
            _context.SaveChanges();
            return RedirectToAction(nameof(ViewVendedor));
        }

        public IActionResult DeleteItem(int id)
        {
            var item = _context.Itens.Find(id);
            if (item == null)
                return RedirectToAction(nameof(ViewItem));
            return View(item);
        }

        [HttpPost]
        public IActionResult DeleteItem(Item item)
        {
            var itemDb = _context.Itens.Find(item.Id);
            _context.Itens.Remove(itemDb);
            _context.SaveChanges();
            return RedirectToAction(nameof(ViewItem));
        }

    }
}